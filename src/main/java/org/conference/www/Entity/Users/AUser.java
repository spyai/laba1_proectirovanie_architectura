package org.conference.www.Entity.Users;

public class AUser {
    protected Long id;
    protected String login;
    protected String password;
    protected Integer status;
    protected String about;
    protected String surname;
    protected String name;

    public Long getId() {
        return id;
    }

    public Integer getStatus() {
        return status;
    }

    public String getAbout() {
        return about;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
