package org.conference.www.Entity.Answers;

public class AnswerLogin implements IAnswerLoginAccount{
    Integer status;
    String message;
    String token;

    @Override
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        Integer status = this.status;
        String message;
        if (status == 0) {
            message = "Login";
        } else {
            message = "auth was failed";
        }
        return message;
    }

    @Override
    public String getAccessToken() {
        return token;
    }

    @Override
    public void setAccessToken(String md5) {
        token = md5;
    }
}
