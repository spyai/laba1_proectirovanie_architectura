package org.conference.www.Entity.Answers;

public class AnswerSign implements IAnswer{
    Integer status;
    String message;

    @Override
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        Integer status = this.status;
        String message = "";
        switch (status){
            case 0:
                message = "Sign up was successfully";
                break;
            case 1:
                message = "Sign up was failed: user already exists";
                break;
            case 2:
                message = "Register was successfully";
                break;
            case 3:
                message = "Register was failed: user already register";
                break;
            case 4:
                message = "Unlegal user login!";
                break;
            case 5:
                message = "Permission denied";
                break;
            default:
                message = "Sign up was failed: unknown error";
        }
        return message;
    }
}
