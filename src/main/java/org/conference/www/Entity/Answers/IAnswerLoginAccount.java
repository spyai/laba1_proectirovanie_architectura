package org.conference.www.Entity.Answers;

public interface IAnswerLoginAccount extends IAnswer {
    String getAccessToken();
    void setAccessToken(String md5);
}
