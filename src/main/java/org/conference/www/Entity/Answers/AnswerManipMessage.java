package org.conference.www.Entity.Answers;

public class AnswerManipMessage implements IAnswer {
    Integer status;
    String message;
    public AnswerManipMessage(Integer status){
        this.status = status;
        message = getMessage();
    }
    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        String res = "";
        if (status == 0) {
            res = "Операция прошла успешно!";
        } else if (status == 5) {
            res = "Недостаточно прав на выполнение операции!";
        }
        else {
            res = "Произошла ошибка!";
        }
        return res;
    }

    @Override
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
