package org.conference.www.Entity.Answers;

public interface IAnswer {
    Integer getStatus();

    String getMessage();

    void setStatus(Integer status);

    void setMessage(String message);
}
