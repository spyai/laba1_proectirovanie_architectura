package org.conference.www.Entity.Messages;

import lombok.Getter;
import lombok.Setter;
import org.conference.www.Entity.Users.AUser;

import java.sql.Date;

public class AMessage {

    private Long id;
    private AUser author;
    private String theme;
    private String text;
    private Date date;
    private Long url;

    public void setId(Long id) {
        this.id = id;
    }

    public AUser getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public Long getId() {
        return id;
    }

    public String getTheme() {
        return theme;
    }

    public Long getUrl() {
        return url;
    }

    public void setAuthor(AUser author) {
        this.author = author;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setUrl(Long url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
