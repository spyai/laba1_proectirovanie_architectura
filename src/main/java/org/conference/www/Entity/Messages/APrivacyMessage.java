package org.conference.www.Entity.Messages;

import lombok.Getter;
import lombok.Setter;
import org.conference.www.Entity.Users.AUser;

public abstract class APrivacyMessage extends AMessage{
    private AUser destinationUser;
}
