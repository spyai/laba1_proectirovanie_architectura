package org.conference.www.Entity.Helpers;

import org.conference.www.Entity.Users.AUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<AUser> {

    @Override
    public AUser mapRow(ResultSet rs, int i) throws SQLException {
        AUser user = new AUser();
        user.setId(rs.getString("id") == null? null : rs.getLong("id"));
        user.setLogin(rs.getString("login") == null? null : rs.getString("login"));
        user.setPassword(rs.getString("password") == null? null : rs.getString("password"));
        user.setStatus(rs.getString("status") == null? null : rs.getInt("status"));
        user.setAbout(rs.getString("about") == null? null : rs.getString("about"));
        user.setName(rs.getString("name") == null? null :  rs.getString("name"));
        user.setSurname(rs.getString("surname") == null? null : rs.getString("surname"));
        return user;
    }
}
