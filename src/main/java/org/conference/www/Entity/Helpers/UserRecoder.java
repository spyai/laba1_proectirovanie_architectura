package org.conference.www.Entity.Helpers;

import org.conference.www.Entity.Users.AUser;

public class UserRecoder {
    public static String getStatus(Integer status_code){
        String status = "";
        switch (status_code){
            case 0: status = "moderator";
            break;
            case 1: status = "register user"; break;
            case 15: status = "unregister user"; break;
            case 10: status = "banned"; break;
            default: status = "unrecognized user"; break;
        }
        return status;
    }
    public static AUser getSafetyProfile(AUser user) {
        AUser res = new AUser();
        res.setLogin(getUserAttribute(user.getLogin()));
        res.setAbout(getUserAttribute(user.getAbout()));
        res.setName(getUserAttribute(user.getName()));
        res.setSurname(getUserAttribute(user.getSurname()));
        return res;
    }
    public static  String getUserAttribute(String login){
        if (login == null) return "";
        else return login.strip();
    }
}