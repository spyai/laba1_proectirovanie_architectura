package org.conference.www.Entity.Helpers;

import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Messages.FullMessage;
import org.conference.www.Entity.Users.AUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RequiresMessagesRowMapper implements RowMapper<FullThreadComment> {
    @Override
    public FullThreadComment mapRow(ResultSet rs, int i) throws SQLException {
        FullMessage message = new FullMessage();
        message.setId(rs.getLong("mid"));
        AUser user = new AUser();

        user.setId(rs.getLong("uid"));
        user.setLogin(rs.getString("login"));
        user.setStatus(rs.getInt("status"));

        message.setAuthor( user );
        message.setDate(rs.getDate("addedDate"));
        message.setTheme(rs.getString("theme"));
        message.setText(rs.getString("text"));
        message.setUrl(rs.getLong("url"));

        FullThreadComment threadComment = new FullThreadComment(message, rs.getInt("depth"));
        return threadComment;
    }
}
