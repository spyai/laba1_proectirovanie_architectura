package org.conference.www.Entity.Helpers;

import org.conference.www.Entity.Users.AUser;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PublicUserRowMap implements RowMapper<AUser> {
    @Override
    public AUser mapRow(ResultSet resultSet, int i) throws SQLException {
        AUser user = new AUser();
        user.setLogin(resultSet.getString("login"));
        user.setStatus(resultSet.getInt("status"));
        user.setAbout(resultSet.getString("about"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));

        return user;
    }
}
