package org.conference.www.Entity.Helpers;

import org.conference.www.Entity.Messages.AMessage;

public class FullThreadComment {
    private AMessage message;
    private Integer depth;

    public FullThreadComment(AMessage message, Integer depth) {
        this.message = message;
        this.depth = depth;
    }

    public AMessage getMessage() {
        return message;
    }

    public void setMessage(AMessage message) {
        this.message = message;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }
}
