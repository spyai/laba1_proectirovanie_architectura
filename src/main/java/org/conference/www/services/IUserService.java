package org.conference.www.services;

import org.conference.www.Entity.Helpers.FullThreadComment;
import org.conference.www.Entity.Messages.AMessage;

import java.util.List;

public interface IUserService {
    List<FullThreadComment> getFullMessage(Long id);

    List<AMessage> getAllMessage();

    List<AMessage> getSearchMessage(String str);
}
