package org.conference.www.services.impl;

import org.conference.www.Entity.Helpers.FullThreadComment;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.dao.IUserDao;
import org.conference.www.services.IUserService;

import java.util.List;

public class UserService implements IUserService {
    protected final IUserDao user;

    public UserService(IUserDao user) {
        this.user = user;
    }

    @Override
    public List<FullThreadComment> getFullMessage(Long id) {
        return user.getFullThread(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<AMessage> getAllMessage() {
        return user.getAllMessage().orElseThrow();
    }

    @Override
    public List<AMessage> getSearchMessage(String str) {
        return user.getMessageSearch(str).orElseThrow();
    }
}
