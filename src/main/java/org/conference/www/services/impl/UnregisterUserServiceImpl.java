package org.conference.www.services.impl;

import org.conference.www.Entity.Answers.AnswerSign;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.dao.IUnregisterUserDao;
import org.conference.www.services.IUnregisterUserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UnregisterUserServiceImpl extends UserService implements IUnregisterUserService {

    public UnregisterUserServiceImpl(IUnregisterUserDao unregisterUser){
        super(unregisterUser);
    }

    @Override
    public IAnswer signup(String login, String password) {
        Optional<IAnswer> req = ((IUnregisterUserDao) user).SignUp(login, password);
        IAnswer ans = new AnswerSign();
        if (req.isEmpty()){
            ans.setStatus(10);
        }
        else {
            ans = req.get();
        }
        ans.setMessage(ans.getMessage());
        return ans;
    }
}
