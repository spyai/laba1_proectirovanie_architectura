package org.conference.www.services.impl;


import org.conference.www.Entity.Answers.AnswerLogin;
import org.conference.www.Entity.Answers.AnswerManipMessage;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.dao.IRegisterDao;
import org.conference.www.services.IRegisterUserService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Primary
public class RegisterServiceImpl extends UserService implements IRegisterUserService {
    public RegisterServiceImpl(IRegisterDao user) {
        super(user);
    }

    @Override
    public IAnswerLoginAccount login(String login, String phrase) {
        IRegisterDao dao = (IRegisterDao) user;
        IAnswerLoginAccount ans = new AnswerLogin();
        Optional<IAnswerLoginAccount> res = dao.login(login, phrase);
        if (res.isEmpty()) {
            ans.setStatus(10);
        }
        else {
            ans = res.get();
        }
        ans.setMessage(ans.getMessage());
        return ans;
    }

    @Override
    public Integer getStatusAccount(String login, IAnswerLoginAccount token) {
        IRegisterDao dao = (IRegisterDao) user;
        return dao.GetStatusAccount(login, token).get();
    }


    private boolean validateUser(AUser author){
        if (author != null){
            if (author.getStatus() > 1) {
                return false;
            }
        }
        else {
            return false;
        }
        return true;
    }
    @Override
    public IAnswer createThread(AMessage message, AUser author) {
        IRegisterDao dao = (IRegisterDao) user;
        IAnswer answer = new AnswerManipMessage(0);
        if (!validateUser(author)){
            answer.setStatus(5);
            answer.setMessage(answer.getMessage());
            return answer;
        }

        message.setAuthor(author);
        Optional<IAnswer> daoRes = dao.createThread(message);
        if (daoRes.isPresent())
            answer = daoRes.get();
        else
            answer.setStatus(10);
        answer.setMessage(answer.getMessage());
        return answer;
    }

    @Override
    public IAnswer sendMessage(Long parent_id, AMessage message, AUser current_user) {
        IRegisterDao dao = (IRegisterDao) user;
        IAnswer answer = new AnswerManipMessage(0);
        if (!validateUser(current_user)){
            answer.setStatus(5);
            answer.setMessage(answer.getMessage());
            return answer;
        }

        message.setAuthor(current_user);
        Optional<IAnswer> daoRes = dao.SendMessage(message, parent_id);
        if (daoRes.isPresent())
            answer = daoRes.get();
        else
            answer.setStatus(10);
        answer.setMessage(answer.getMessage());

        return answer;
    }

    @Override
    public Long getID(String login) {
        IRegisterDao dao = (IRegisterDao) user;
        Optional<Long> res = dao.GetIDAccount(login);
        return res.orElseThrow(RuntimeException::new);
    }

    @Override
    public IAnswer deleteMessage(Long idMessage, AUser current_user) {
        IRegisterDao dao = (IRegisterDao) user;
        IAnswer answer = new AnswerManipMessage(0);
        if (!validateUser(current_user)){
            answer.setStatus(5);
            answer.setMessage(answer.getMessage());
            return answer;
        }


        Optional<IAnswer> daoRes = dao.deleteMessage( idMessage );
        if (daoRes.isPresent())
            answer = daoRes.get();
        else
            answer.setStatus(10);
        answer.setMessage(answer.getMessage());

        return answer;
    }

    @Override
    public AUser getProfile(String login) {
        IRegisterDao dao = (IRegisterDao) user;
        Optional<AUser> opt = dao.getProfile(login);
        return opt.orElse(null);

    }

    @Override
    public Boolean updateProfile(String login, AUser profile) {
        IRegisterDao dao = (IRegisterDao) user;
        Optional<Boolean> opt = dao.updateProfile(login, profile);
        return opt.orElse(null);
    }

}
