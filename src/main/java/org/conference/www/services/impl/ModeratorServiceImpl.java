
package org.conference.www.services.impl;

import org.conference.www.Entity.Answers.AnswerManipMessage;
import org.conference.www.Entity.Answers.AnswerSign;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.dao.IModeratorDao;
import org.conference.www.services.IModeratorService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ModeratorServiceImpl extends RegisterServiceImpl implements IModeratorService {

    private final IModeratorDao moderatorDao;

    public ModeratorServiceImpl(IModeratorDao moderator){
        super(moderator);
        this.moderatorDao = moderator;
    }

    private boolean isForbidden(AUser moder, IAnswerLoginAccount status) {
        if (moder == null) {
            return true;
        }
        Optional<Integer> statusAccount = moderatorDao.GetStatusAccount(moder.getLogin(), status);
        if (statusAccount.isEmpty())
            return true;
        return statusAccount.get() != 0;
    }


    @Override
    public IAnswer RegisterUser(AUser unregisterUser, AUser moder, IAnswerLoginAccount token) {
        IAnswer ans = new AnswerSign();
        if (isForbidden(moder, token))
            ans.setStatus(5);
        else {
            Optional<IAnswer> req = moderatorDao.SignInNewUser(unregisterUser);
            if (req.isEmpty()){
                ans.setStatus(10);
            }
            else {
                ans = req.get();
            }
        }
        ans.setMessage(ans.getMessage());
        return ans;
    }
    @Override
    public List<AUser> getListUnregisterUser(AUser moder, IAnswerLoginAccount token) {
        if (isForbidden(moder, token))
            return null;

        List<AUser> result = null;
        Optional<List<AUser>> ans = moderatorDao.GetListUnregisterUser();
        if (ans.isEmpty())
            return null;
        else
            result = ans.get();

        return result;
    }
    @Override
    public List<AUser> getListRegisterUser(AUser moder, IAnswerLoginAccount token) {
        if (isForbidden(moder, token))
            return null;

        List<AUser> result = null;
        Optional<List<AUser>> ans = moderatorDao.GetListRegisterUser();
        if (ans.isEmpty())
            return null;
        else
            result = ans.get();

        return result;
    }

    @Override
    public IAnswer blockRegisterUser(AUser registerUser, AUser moder, IAnswerLoginAccount token) {
        IAnswer ans = new AnswerManipMessage(0);
        if (isForbidden(moder, token))
            ans.setStatus(5);
        else {
            Optional<IAnswer> req = moderatorDao.BlockRegisterUser(registerUser);
            if (req.isEmpty()){
                ans.setStatus(10);
            }
            else {
                ans = req.get();
            }
        }
        ans.setMessage(ans.getMessage());
        return ans;
    }

    @Override
    public IAnswer unblockRegisterUser(AUser registerUser, AUser moder, IAnswerLoginAccount token) {
        IAnswer ans = new AnswerManipMessage(0);
        if (isForbidden(moder, token))
            ans.setStatus(5);
        else {
            Optional<IAnswer> req = moderatorDao.UnblockRegisterUser(registerUser);
            if (req.isEmpty()){
                ans.setStatus(10);
            }
            else {
                ans = req.get();
            }
        }
        ans.setMessage(ans.getMessage());
        return ans;
    }
}
