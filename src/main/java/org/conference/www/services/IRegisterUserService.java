package org.conference.www.services;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;

public interface IRegisterUserService extends IUserService{
    IAnswerLoginAccount login(String login, String phrase);

    Integer getStatusAccount(String login, IAnswerLoginAccount token);

    IAnswer createThread(AMessage message, AUser author);

    IAnswer sendMessage(Long parent_id, AMessage message,  AUser user);

    Long getID(String login);

    IAnswer deleteMessage(Long idMessage, AUser user);

    AUser getProfile(String login);

    Boolean updateProfile(String login, AUser profile);
}
