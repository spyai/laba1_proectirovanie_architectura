package org.conference.www.services;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Messages.AMessage;

public interface IUnregisterUserService extends IUserService{
    IAnswer signup(String login, String password);
}
