package org.conference.www.services;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Users.AUser;

import java.util.List;

public interface IModeratorService extends IRegisterUserService {
    IAnswer RegisterUser(AUser unregisterUser, AUser moder, IAnswerLoginAccount status);
    List<AUser> getListUnregisterUser(AUser moder, IAnswerLoginAccount token);
    List<AUser> getListRegisterUser(AUser moder, IAnswerLoginAccount token);
    IAnswer blockRegisterUser(AUser registerUser, AUser moder, IAnswerLoginAccount token);
    IAnswer unblockRegisterUser(AUser registerUser, AUser moder, IAnswerLoginAccount token);
}
