package org.conference.www.controller;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.services.IUnregisterUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/new")
public class UnregisterUserController {
    private final IUnregisterUserService service;
    @Autowired
    public UnregisterUserController(IUnregisterUserService service) {
        this.service = service;
    }

    @GetMapping(value = {"", "/"})
    public String registerUserForm(Model model) {
        model.addAttribute("user", new AUser());
        return "signup-page";
    }

    @PostMapping(value = "/signup")
    public String signup(@ModelAttribute AUser user, HttpServletRequest request){
        IAnswer ans = service.signup(user.getLogin(), user.getPassword());
        HttpSession session = request.getSession();
        Integer status = ans.getStatus();
        session.setAttribute("status_sign", status);
        if (status == 0) {
            return "redirect:/";
        }
        return "redirect:/new";
    }
}
