package org.conference.www.controller;



import org.conference.www.Entity.Helpers.FullThreadComment;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Messages.FullMessage;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.services.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/")
public class UserController {
    protected final IUserService service;

    public UserController(IUserService service) {
        this.service = service;
    }

    @GetMapping(value = {"", "/"})
    public String redirectToMessage(){
        return "redirect:message";
    }

    @GetMapping(value = {"/message/{id}", "/message/{id}/"})
    public String getFullMessage(@PathVariable Long id, Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        try{
            List<FullThreadComment> ans = service.getFullMessage(id);
            if (ans.isEmpty()){
                return "501";
            }
            else {

                List<FullThreadComment> comments = ans.stream()
                        .dropWhile(aMessage -> Objects.equals(aMessage.getMessage().getId(), ans.get(0).getMessage().getId()))
                        .collect(Collectors.toList());
                AUser user = (AUser) session.getAttribute("user");

                model.addAttribute("title", ans.get(0).getMessage());
                model.addAttribute("comments", comments);
                model.addAttribute("message", new FullMessage());
            }
            return "get-message-page";
        }
        catch (RuntimeException e){
            return "501";
        }
    }

    @GetMapping(value = {"/message", "/message/"})
    public String getAllMessage(Model model, HttpServletRequest request){
        List<AMessage> ans = service.getAllMessage();
        HttpSession httpSession = request.getSession();
        model.addAttribute("messages", ans);
        model.addAttribute("newmessage", new FullMessage());
        return "main-page";
    }

    @GetMapping(value = {"/search"})
    public String search(@RequestParam String s, Model model){

        List<AMessage> lst = service.getSearchMessage(s);
        model.addAttribute("messages", lst);
        return "search-page";
    }

}
