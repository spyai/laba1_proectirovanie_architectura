package org.conference.www.controller;

import org.conference.www.Entity.Answers.AnswerLogin;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.services.IModeratorService;
import org.conference.www.services.impl.ModeratorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping(value = "/moder", produces = MediaType.APPLICATION_JSON_VALUE)
public class ModeratorController {
    private final IModeratorService service;
    private enum Page {
        UnregisterPage, RegisterPage, BanPage
    }
    private IAnswerLoginAccount isForbidden(HttpSession session){
        Integer status_login = (Integer) session.getAttribute("status_login");
        if (status_login != null){
            if (status_login == 0 && (Integer)session.getAttribute("status_account") > 0){
                return null;
            }
        }
        else {
            return null;
        }
        if (session.getAttribute("user") == null) {
            return null;
        }

        IAnswerLoginAccount token = new AnswerLogin();
        token.setStatus((Integer) session.getAttribute("status_login"));
        token.setAccessToken((String) session.getAttribute("token"));

        return token;

    }


    @Autowired
    public ModeratorController(ModeratorServiceImpl service) {
        this.service = service;
    }

    @GetMapping(value = {"/", ""})
    public String moderPage(){
        return "redirect:/moder/unreg";
    }

    @GetMapping(value = "/access/{moder}/{login}")
    public String GetAccessForUser(@PathVariable("login") String loginAuth, @RequestParam("sign") String token,
                                    @PathVariable("moder") String moderatorLogin){

        AnswerLogin answerLogin = new AnswerLogin();
        if (Objects.equals(token, "-1")){
            answerLogin.setStatus(1);
        }
        else
            answerLogin.setStatus(0);
        answerLogin.setAccessToken(token);

        AUser user = new AUser();
        user.setLogin(loginAuth);

        AUser moder = new AUser();
        moder.setLogin(moderatorLogin);
        IAnswer ans = service.RegisterUser(user, moder, answerLogin);

        return "redirect:/moder";
    }
    @GetMapping(value = "/ban/{login}")
    public String blockRegisterUser(@PathVariable("login") String login,
                                    Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        IAnswerLoginAccount token = isForbidden(session);
        if (token == null) return "403";
        AUser moder = (AUser) session.getAttribute("user");
        AUser bannedUser = new AUser();
        bannedUser.setLogin(login);
        IAnswer ans = service.blockRegisterUser(bannedUser, moder, token);
        if (ans.getStatus() == 0)
            return "redirect:/moder/reg";
        else {
            return "501";
        }
    }
    @GetMapping(value = "/unban/{login}")
    public String unblockRegisterUser(@PathVariable("login") String login,
                                    Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        IAnswerLoginAccount token = isForbidden(session);
        if (token == null) return "403";
        AUser moder = (AUser) session.getAttribute("user");
        AUser bannedUser = new AUser();
        bannedUser.setLogin(login);
        IAnswer ans = service.unblockRegisterUser(bannedUser, moder, token);
        if (ans.getStatus() == 0)
            return "redirect:/moder/reg";
        else {
            return "501";
        }
    }

    @GetMapping(value = {"/unreg", "/unreg/"})
    public String moderPage(Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        IAnswerLoginAccount token = isForbidden(session);

        if (token == null) return "403";
        AUser moder = (AUser) session.getAttribute("user");

        List<AUser> userList =  service.getListUnregisterUser(moder, token);
        model.addAttribute("page", Page.UnregisterPage.ordinal());
        model.addAttribute("users", userList);
        model.addAttribute("moder_login", moder.getLogin());
        model.addAttribute("sign", token.getAccessToken());

        return "moderator-page";
    }

    @GetMapping(value = {"/reg", "/reg/"})
    public String moderRegPage(Model model, HttpServletRequest request){
        HttpSession session = request.getSession();
        IAnswerLoginAccount token = isForbidden(session);

        if (token == null) return "403";
        AUser moder = (AUser) session.getAttribute("user");

        List<AUser> userList =  service.getListRegisterUser(moder, token);

        model.addAttribute("page", Page.RegisterPage.ordinal());
        model.addAttribute("users", userList);



        return "moderator-page";
    }
}
