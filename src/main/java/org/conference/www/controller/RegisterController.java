package org.conference.www.controller;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Helpers.UserRecoder;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.services.IRegisterUserService;
import org.conference.www.services.impl.RegisterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

@Controller
@RequestMapping(value = "/reg")
public class RegisterController {

    protected final IRegisterUserService service;
    @Autowired
    public RegisterController(RegisterServiceImpl service) {
        this.service = service;
    }

    @GetMapping(value = {"/", ""})
    public String getFormLogin(Model model){
        model.addAttribute("user", new AUser());
        return "auth-page";
    }

    @GetMapping(value = {"/logout", "/logout/"})
    public String logout(HttpServletRequest req){
        HttpSession session = req.getSession();

        session.removeAttribute("token");
        session.removeAttribute("status_login");
        session.removeAttribute("user");
        session.removeAttribute("status_account");
        session.removeAttribute("isPopupAuthShow");
        session.removeAttribute("permission");
        return "redirect:/message";
    }

    @GetMapping(value = {"/profile", "/profile/"})
    public String getProfile(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        if (session.getAttribute("status_account") == null) {
            return "redirect:/";
        }
        AUser aUser = (AUser) session.getAttribute("user");
        AUser profile = service.getProfile(aUser.getLogin());
        if (profile == null)
            return "redirect:/";
        model.addAttribute("form_user", UserRecoder.getSafetyProfile( profile ));
        model.addAttribute("status", UserRecoder.getStatus( profile.getStatus() ));

        return "profile-page";
    }

    @PostMapping(value = "/auth")
    public String loginAccount(@ModelAttribute AUser user, HttpServletRequest request) {
        IAnswerLoginAccount ans = service.login(user.getLogin(), user.getPassword());
        HttpSession session = request.getSession();
        user.setStatus(service.getStatusAccount(user.getLogin(), ans));
        user.setId(service.getID(user.getLogin()));
        if (ans.getStatus() == 0) {
            // сохранение в сессию токена
            session.setAttribute("token", ans.getAccessToken());
            session.setAttribute("status_login", ans.getStatus());
            session.setAttribute("user", user);
            session.setAttribute("status_account", user.getStatus());
            session.setAttribute("isPopupAuthShow", false);
            session.removeAttribute("permission");
            return "redirect:/message";
        }
        else {
            session.setAttribute("status_login", ans.getStatus());
            return "redirect:/reg";
        }
    }

    @PostMapping(value = "/send/message")
    public String sendMessage(@ModelAttribute AMessage message, @RequestParam(value = "mid") Long parent_id,
                              HttpServletRequest request){
        HttpSession session = request.getSession();
        if (session.getAttribute("permission") != null
                && !(Boolean) session.getAttribute("permission")) {
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }

        AUser sender = (AUser) session.getAttribute("user");
        IAnswer ans = service.sendMessage(parent_id, message, sender);


        if (ans.getStatus() == 0) {
            session.setAttribute("permission", true);
            return "redirect:" + request.getHeader("referer");
        }
        else if (ans.getStatus() == 5){
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }
        else
            return "501";
    }

    @PostMapping(value = "/thread")
    public String createThread(@ModelAttribute AMessage message, HttpServletRequest request){
        HttpSession session = request.getSession();
        if (session.getAttribute("permission") != null && !(Boolean) session.getAttribute("permission")) {
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }
        AUser sender = (AUser) session.getAttribute("user");
        IAnswer ans = service.createThread(message, sender);


        if (ans.getStatus() == 0) {
            session.setAttribute("permission", true);
            return "redirect:" + request.getHeader("referer");
        }
        else if (ans.getStatus() == 5){
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }
        else
            return "501";

    }

    @GetMapping(value = "/delete")
    public String deleteMessage(@RequestParam Long id, HttpServletRequest request){
        HttpSession session = request.getSession();
        AUser sender = (AUser) session.getAttribute("user");
        IAnswer ans = service.deleteMessage(id, sender);

        if (ans.getStatus() == 0) {
            session.setAttribute("permission", true);
            return "redirect:" + request.getHeader("referer");
        }
        else if (ans.getStatus() == 5){
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }
        else
            return "501";
    }
    @GetMapping(value = "/delete/thread")
    public String deleteThread(@RequestParam Long id, HttpServletRequest request){
        HttpSession session = request.getSession();
        AUser sender = (AUser) session.getAttribute("user");
        IAnswer ans = service.deleteMessage(id, sender);

        if (ans.getStatus() == 0) {
            session.setAttribute("permission", true);
            return "redirect:/";
        }
        else if (ans.getStatus() == 5){
            session.setAttribute("permission", false);
            return "redirect:" + request.getHeader("referer");
        }
        else
            return "501";
    }

    @PostMapping(value = "/update")
    public String updateUser(@ModelAttribute AUser model, HttpServletRequest request){
        HttpSession session = request.getSession();
        model.setLogin(model.getLogin().toLowerCase(Locale.ROOT).strip());
        model.setSurname(model.getSurname().toLowerCase(Locale.ROOT).strip());
        model.setAbout(model.getAbout().toLowerCase(Locale.ROOT).strip());
        model.setName(model.getName().toLowerCase(Locale.ROOT).strip());
        AUser sender = (AUser) session.getAttribute("user");
        if (sender == null)
            return "redirect:" + request.getRequestURI();
        String etalon = sender.getLogin();

        service.updateProfile(etalon, model);
        sender.setLogin(model.getLogin());
        session.setAttribute("user", sender);
        return "redirect:/";
    }
}
