package org.conference.www.dao.implement;

import org.conference.www.Entity.Answers.AnswerManipMessage;
import org.conference.www.Entity.Answers.AnswerSign;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Helpers.MessageRowMapper;
import org.conference.www.Entity.Helpers.UserRowMapper;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.dao.IModeratorDao;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class Moderator extends RegisterUserDao implements IModeratorDao {

    private final static String SQL_REGISTER_USER =
        "select registeruser(:login)";
    private  final static String SQL_SELECT_UNREGISTER_USER =
            "select * from public.user where status = :status order by id asc";
    private final static String SQL_SELECT_REGISTER_USER =
            "select * from public.user where status = 1 or status = 10 order by id asc";
    private final static String SQL_BLOCK_USER =
            "select blockuser(:login)";
    private final static String SQL_UNBLOCK_USER =
            "select unblockuser(:login)";
    public Moderator(UserRowMapper mapper, MessageRowMapper messageMapper, NamedParameterJdbcTemplate jdbcTemplate){
        super(mapper, messageMapper, jdbcTemplate);
    }

    @Override
    public Optional<List<AUser>> GetListUnregisterUser() {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status", 15); // не прошедший модератора пользователь
        try{
            return Optional.of(
                jdbcTemplate.query(SQL_SELECT_UNREGISTER_USER, parameterSource, this.userMapper)
            );
        }
        catch (DataAccessException exception){
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<AUser>> GetListRegisterUser() {
        try{
            return Optional.of(
                    jdbcTemplate.query(SQL_SELECT_REGISTER_USER, this.userMapper)
            );
        }
        catch (DataAccessException exception){
            return Optional.empty();
        }
    }

    @Override
    public Optional<IAnswer> SignInNewUser(AUser user) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", user.getLogin());
        try{
            Integer res = jdbcTemplate.queryForObject(SQL_REGISTER_USER, parameterSource, Integer.class);
            AnswerSign answerSign = new AnswerSign();
            answerSign.setStatus(res + 2);
            return Optional.of(answerSign);
        }
        catch (DataAccessException | NullPointerException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<IAnswer> BlockRegisterUser(AUser user) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", user.getLogin());
        try{
            Integer res = jdbcTemplate.queryForObject(SQL_BLOCK_USER, parameterSource, Integer.class);
            AnswerManipMessage answerSign = new AnswerManipMessage(res);
            return Optional.of(answerSign);
        }
        catch (DataAccessException | NullPointerException e) {
            return Optional.empty();
        }
    }
    @Override
    public Optional<IAnswer> UnblockRegisterUser(AUser user) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", user.getLogin());
        try{
            Integer res = jdbcTemplate.queryForObject(SQL_UNBLOCK_USER, parameterSource, Integer.class);
            AnswerManipMessage answerSign = new AnswerManipMessage(res);
            return Optional.of(answerSign);
        }
        catch (DataAccessException | NullPointerException e) {
            return Optional.empty();
        }
    }
}
