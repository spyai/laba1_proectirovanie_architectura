package org.conference.www.dao.implement;

import org.conference.www.Entity.Answers.AnswerLogin;
import org.conference.www.Entity.Answers.AnswerManipMessage;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Helpers.MessageRowMapper;
import org.conference.www.Entity.Helpers.PublicUserRowMap;
import org.conference.www.Entity.Helpers.UserRowMapper;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.dao.IRegisterDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Primary
public class RegisterUserDao extends UserDao implements IRegisterDao {
    public RegisterUserDao(UserRowMapper mapper, MessageRowMapper messageMapper, NamedParameterJdbcTemplate jdbcTemplate){
        super(mapper, messageMapper, jdbcTemplate);
    }
    private final Logger logger = LoggerFactory.getLogger(RegisterUserDao.class);
    private final static String SQL_GETID = "select id from public.user where login = :login";
    private final static String SQL_LOGIN_REQUEST = "select login(:login, :phrase)";
    private final static String SQL_GETSTATUS_REQUEST = "select getstatus(:login, :md5)";
    private final static String SQL_SENDMESSAGE_REQUEST =
            "select sendmessage(:parent_id, :author, :theme, :text)";
    private final static String SQL_CREATE_THREAD_REQUEST =
            "select createthread(:author, :theme, :text)";
    private final static String SQL_DELETEMESSAGE_REQUEST =
            "select deletemessage(:id)";
    private final static String SQL_GET_PUBLIC_USER = "select * from public_user where login = :login";
    private final static String SQL_UPDATE_PROFILE = "select  update_profile(:etalon, :login, :about, " +
            ":name, :surname)";

    @Override
    public Optional<IAnswerLoginAccount> login(String login, String phrase) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source
            .addValue("login", login)
            .addValue("phrase", phrase);
        try{
            String responce = jdbcTemplate.queryForObject(SQL_LOGIN_REQUEST, source, String.class);
            IAnswerLoginAccount result = new AnswerLogin();
            if (responce != null){
                result.setStatus(0);
                result.setAccessToken(responce);
            } else {
                result.setStatus(1);
            }
            return Optional.of(result);
        }
        catch (DataAccessException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<IAnswer> SendMessage(AMessage message, Long parent_id) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("parent_id", parent_id);
        return getiAnswer(message, source, SQL_SENDMESSAGE_REQUEST);
    }

    @Override
    public Optional<IAnswer> createThread(AMessage message) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        return getiAnswer(message, source, SQL_CREATE_THREAD_REQUEST);
    }

    private Optional<IAnswer> getiAnswer(AMessage message, MapSqlParameterSource source, String request) {
        source.addValue("author", message.getAuthor().getId());
        source.addValue("theme", message.getTheme());
        source.addValue("text", message.getText());
        try{
            IAnswer answer = new AnswerManipMessage(
                    Boolean.TRUE.equals(jdbcTemplate.queryForObject(request, source, Boolean.class)) ?
                            0 :
                            10);
            return Optional.of(answer);
        }
        catch (DataAccessException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<IAnswer> deleteMessage(Long idMessage) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("id", idMessage);
        try{
            return Optional.of(
                new AnswerManipMessage(
                        Boolean.TRUE.equals(jdbcTemplate.queryForObject(SQL_DELETEMESSAGE_REQUEST, source, Boolean.class)) ?
                        0 :
                        10)
            );
        }
        catch (DataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public Optional<Integer> GetStatusAccount(String login, IAnswerLoginAccount status) {
        if (status.getStatus() != 0) return Optional.of(-1);
        MapSqlParameterSource source = new MapSqlParameterSource();
        source
                .addValue("login", login)
                .addValue("md5", status.getAccessToken());
        try {
            return Optional.ofNullable(
                    jdbcTemplate.queryForObject(SQL_GETSTATUS_REQUEST, source, Integer.class)
            );
        }
        catch (DataAccessException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }

    }

    @Override
    public Optional<Long> GetIDAccount(String login) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("login", login);
        try{
            return Optional.ofNullable(
                jdbcTemplate.queryForObject(SQL_GETID, source, Long.class)
            );
        }
        catch (DataAccessException e){
            logger.error(e.getMessage());
            return Optional.empty();
        }

    }

    @Override
    public Optional<AUser> getProfile(String login) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("login", login);
        try{
            return Optional.ofNullable(
                    jdbcTemplate.queryForObject(SQL_GET_PUBLIC_USER, source,new PublicUserRowMap())
            );
        }
        catch (DataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public Optional<Boolean> updateProfile(String login, AUser profile) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("etalon", login);
        source.addValue("login", profile.getLogin());
        source.addValue("about", profile.getAbout());
        source.addValue("name", profile.getName());
        source.addValue("surname", profile.getSurname());

        try{
            return Optional.ofNullable(
                jdbcTemplate.queryForObject(SQL_UPDATE_PROFILE, source, Boolean.class)
            );
        }
        catch (DataAccessException ex) {
            return Optional.empty();
        }
    }
}
