package org.conference.www.dao.implement;


import org.conference.www.Entity.Answers.AnswerSign;
import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Helpers.MessageRowMapper;
import org.conference.www.Entity.Helpers.UserRowMapper;
import org.conference.www.dao.IUnregisterUserDao;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UnregisterUserDao extends UserDao implements IUnregisterUserDao {

    private static final String SQL_SIGN_UP_USER =
            "select signup(:login, :passphrase)";

    public UnregisterUserDao(UserRowMapper mapper, MessageRowMapper messageMapper, NamedParameterJdbcTemplate jdbcTemplate){
        super(mapper, messageMapper, jdbcTemplate);
    }
    @Override
    public Optional<IAnswer> SignUp(String login, String passphrase) {
        MapSqlParameterSource source = new MapSqlParameterSource();
        source
            .addValue("login", login)
            .addValue("passphrase", passphrase);
        try{
            Integer result = jdbcTemplate.queryForObject(SQL_SIGN_UP_USER, source, Integer.class);
            AnswerSign answerSign = new AnswerSign();
            answerSign.setStatus(result);
            return Optional.of( answerSign );
        }
        catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
