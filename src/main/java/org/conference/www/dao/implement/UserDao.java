package org.conference.www.dao.implement;

import org.conference.www.Entity.Helpers.FullThreadComment;
import org.conference.www.Entity.Helpers.MessageRowMapper;
import org.conference.www.Entity.Helpers.RequiresMessagesRowMapper;
import org.conference.www.Entity.Helpers.UserRowMapper;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;
import org.conference.www.dao.IUserDao;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDao implements IUserDao {

    private static final String SQL_GET_THREAD_ID =
            "select * from getFullThread(:id)";
    private static final String SQL_GET_THREADS =
            "select * from thread_view";
    private static final String SQL_GET_SEARCHING_MESSAGE =
            "select * from search(:str)";


    protected final RowMapper<AUser> userMapper;
    private final RowMapper<AMessage> messageMapper;
    protected final NamedParameterJdbcTemplate jdbcTemplate;

    public UserDao(UserRowMapper mapper, MessageRowMapper messageMapper, NamedParameterJdbcTemplate jdbcTemplate){
        this.userMapper = mapper;
        this.messageMapper = messageMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Optional<List<AMessage>> getMessageSearch(String str) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("str", str);
        try{
            return Optional.of(
                    jdbcTemplate.query( SQL_GET_SEARCHING_MESSAGE, params, messageMapper)
            );
        }
        catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<FullThreadComment>> getFullThread(Long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        try{
            return Optional.of(
                    jdbcTemplate.query(SQL_GET_THREAD_ID, params, new RequiresMessagesRowMapper())
            );
        }
        catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<AMessage>> getAllMessage() {
        try{
            List<AMessage> messages = jdbcTemplate.query(SQL_GET_THREADS, this.messageMapper);
            return Optional.of( messages );
        }
        catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
