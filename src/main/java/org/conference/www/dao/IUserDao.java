package org.conference.www.dao;

import org.conference.www.Entity.Helpers.FullThreadComment;
import org.conference.www.Entity.Messages.AMessage;

import java.util.List;
import java.util.Optional;

public interface IUserDao {

    Optional<List<AMessage>> getMessageSearch(String str);

    // Поиск сообщения по идентификатору
    Optional<List<FullThreadComment>> getFullThread(Long id);

    // Добавить функцию для отображения всех сообщений простой селект
    Optional<List<AMessage>> getAllMessage();
}
