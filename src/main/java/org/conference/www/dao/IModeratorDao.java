package org.conference.www.dao;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Users.AUser;

import java.util.List;
import java.util.Optional;

public interface IModeratorDao extends IRegisterDao {
     Optional<List<AUser>> GetListUnregisterUser();

     Optional<List<AUser>> GetListRegisterUser();

     Optional<IAnswer>  SignInNewUser(AUser user);

     Optional<IAnswer>  BlockRegisterUser(AUser user);
     Optional<IAnswer> UnblockRegisterUser(AUser user);
}
