package org.conference.www.dao;

import org.conference.www.Entity.Answers.IAnswer;
import org.conference.www.Entity.Answers.IAnswerLoginAccount;
import org.conference.www.Entity.Messages.AMessage;
import org.conference.www.Entity.Users.AUser;

import java.util.Optional;

public interface IRegisterDao extends IUserDao {

    Optional<IAnswerLoginAccount> login(String login, String phrase);

    Optional<IAnswer> SendMessage(AMessage message, Long parent_id);

    Optional<IAnswer> createThread(AMessage message);

    Optional<IAnswer> deleteMessage(Long idMessage);

    Optional<Integer> GetStatusAccount(String login, IAnswerLoginAccount status);

    Optional<Long> GetIDAccount(String login);

    Optional<AUser> getProfile(String login);

    Optional<Boolean> updateProfile(String login, AUser profile);
}
