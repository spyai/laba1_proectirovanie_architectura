package org.conference.www.dao;

import org.conference.www.Entity.Answers.IAnswer;

import java.util.Optional;

public interface IUnregisterUserDao extends IUserDao {

    // Незарегистрированный пользователь пытается зарегистрироваться
    Optional<IAnswer> SignUp(String login, String passphrase);

}
