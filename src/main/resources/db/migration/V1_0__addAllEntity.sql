CREATE TABLE public."user"
(
    id SERIAL NOT NULL,
    login character(70) COLLATE pg_catalog."default" NOT NULL,
    password character(100) COLLATE pg_catalog."default" NOT NULL,
    status integer NOT NULL,
    about character(500) COLLATE pg_catalog."default",
    name character(40) COLLATE pg_catalog."default",
    surname character(40) COLLATE pg_catalog."default",
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.message (
	id serial4 NOT NULL,
	author int8 NOT NULL,
	theme text NOT NULL,
	"text" text NULL,
	"addedDate" date NULL,
	url int8 NULL,
	CONSTRAINT message_pkey PRIMARY KEY (id),
	CONSTRAINT message_comments FOREIGN KEY (url) REFERENCES public.message(id) ON UPDATE CASCADE on DELETE CASCADE,
	CONSTRAINT message_user_fk FOREIGN KEY (author) REFERENCES public."user"(id) ON UPDATE CASCADE
);

