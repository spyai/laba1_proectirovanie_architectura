create table answer (
    status int not null,
    message character varying(50),
    CONSTRAINT answer_pkey primary key (status)
);