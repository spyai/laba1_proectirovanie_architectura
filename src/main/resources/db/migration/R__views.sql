-- public.thread_view source

CREATE OR REPLACE VIEW public.thread_view
AS SELECT m.id as mid,
	u.id as uid,
	u.login,
	u.status,
    m.author,
    m.theme,
    m.text,
    m."addedDate",
    m.url
   FROM message m
   inner join public."user" u on u.id = m.author
WHERE m.url IS NULL;

CREATE OR REPLACE VIEW public.public_user
AS select u.login, u.status, u.about, u."name", u.surname from public."user" u;