
-- получить полный список потока конференции
CREATE OR REPLACE FUNCTION public.getfullthread(id_thread bigint)
 RETURNS TABLE(mid integer, uid integer, login character, status integer, author bigint, theme text, text text, "addedDate" date, url bigint, depth int4)
 LANGUAGE plpgsql
AS $function$
	BEGIN
		return query WITH RECURSIVE message2comment(id, author, theme, "text", "addedDate", url, path) AS (
                     		    SELECT m.id, m.author, m.theme, m."text", m."addedDate", m.url, array[m.id]::bigint[] FROM public.message m WHERE id = id_thread
                     		  		union ALL
                     		    SELECT m.id, m.author, m.theme, m."text", m."addedDate", m.url, m2c.path || array[m.id]::bigint[]
                     		    FROM public.message m inner join message2comment m2c
                     		    on m.url = m2c.id
                     		)
                     		SELECT m2c.id as mid, u.id as uid,u.login, u.status, m2c.author, m2c.theme, m2c."text", m2c."addedDate", m2c.url, array_length(m2c.path,1) as depth
                     		FROM message2comment m2c
                     		inner join public.user u on u.id = m2c.author
                     		order by path;
	END;
$function$
;




-- 0 - пользователя нет, заявка подана; 1 - пользователь есть заявка не будет обработана. Код заявки 15
CREATE OR REPLACE FUNCTION public.signup(login_new character, pass character)
	RETURNS int4
	LANGUAGE plpgsql
AS $function$
declare
uid int;
	BEGIN
		select count(id) into uid from public.user u where login = login_new ;
		if uid = 0 then
			insert into public.user ("login", "password", status) values (login_new, MD5(MD5(pass)), 15);
		end if;
		return uid;

	END;
$function$;

-- 0 - регистрация прошла успешно, 1 - пользователь был уже зарегистрирован, 2 - защита от дурака (себя)
CREATE OR REPLACE FUNCTION public.registeruser(login_user character)
	RETURNS int4
	LANGUAGE plpgsql
AS $function$
declare
stat int8;
res int4;
begin
	res := 0;
	select status into stat from public.user where login = login_user;
	if not FOUND then
		res := 2;
	elsif stat <> 15 then
		res := 1;
	else
		update public.user as u set status = 1 where login = login_user;
		res := 0;
	end if;

	return res;
END;
$function$
;

-- Получение статуса для проверки на права модератора
CREATE OR REPLACE FUNCTION public.getstatus(login_user character, md5_check character)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
declare
hashsum character(40) ;
tmp boolean;
stat int4;
	BEGIN
		select MD5(md5(login || "password")) into hashsum from public.user where login = login_user;
		if hashsum is null then
			tmp := false;
		else
			tmp := (hashsum = md5_check)::boolean ;
		end if;
		if tmp then
			select status into stat from public.user where login = login_user;
		else
			stat := null;
		end if;
		return stat;
	END;
$function$
;

-- Логин пользователя в системе: NULL - не успешный логин, md5(md5(login || password_md5)) - успешный вход // сделать проверку на статус 15
CREATE OR REPLACE FUNCTION public.login(login_user character, passphrase character)
	RETURNS bpchar
	LANGUAGE plpgsql
AS $function$
declare
goal bpchar;
passmd5 bpchar;
	begin
		select md5(md5(passphrase)) into passmd5;
		select (login || "password") into goal from public.user where login = login_user and "password" = passmd5;
		if not found then
			return NULL;
		end if;
		return md5(md5(goal));
	END;
$function$
;
CREATE OR REPLACE FUNCTION public.search(str text)
 RETURNS TABLE(mid int,  theme text, "text" text, addedDate date, url bigint, login bpchar(70), status int4, uid bigint)
 LANGUAGE plpgsql
AS $function$
	BEGIN

		return query select m.id as mid, m.theme, m."text", m."addedDate", m.url, u.login, u.status, m.author  as uid
		from message m inner join public.user u ON u.id = m.author
		where m.theme ~* str or m."text"  ~* str;
	END;
$function$
;
-- Сохранение комментария в базе данных
CREATE OR REPLACE FUNCTION public.sendmessage(parent_id bigint, author bigint, theme text, message text)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
BEGIN
insert into public.message (author, theme, "text", "addedDate", url)
values (author, theme, message, now(), parent_id);
return true;
END;
$function$
;
-- Сохранение потока в базе данных
CREATE OR REPLACE FUNCTION public.createthread(author bigint, theme text, message text)
  RETURNS boolean
  LANGUAGE plpgsql
 AS $function$
 begin
 insert into public.message (author, theme, "text", "addedDate", url)
 values (author, theme, message, now(), null);
 return true;

END;
$function$
;

CREATE OR REPLACE FUNCTION public.deletemessage(mid int8)
	RETURNS bool
	LANGUAGE plpgsql
AS $function$
	BEGIN
		delete from message m where mid = m.id;
		return true;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.updatemessage(mid int8, theme_u text, text_u text)
    RETURNS bool
    LANGUAGE plpgsql
AS $function$
BEGIN
    update public.message set theme = theme_u, "text" = text_u, "addedDate" = now() where id = mid;
    return true;
END;
$function$
;

CREATE OR REPLACE FUNCTION public.blockuser(login_user character)
	RETURNS int4
	LANGUAGE plpgsql
AS $function$
	BEGIN
		update public."user" set status = 10 where login = login_user;
		return 0;
	END;
$function$
;
CREATE OR REPLACE FUNCTION public.unblockuser(login_user character)
	RETURNS int4
	LANGUAGE plpgsql
AS $function$
	BEGIN
		update public."user" set status = 1 where login = login_user;
		return 0;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.update_profile(etalon varchar(70), login_u varchar(70), about_u text, name_u text, surname_u text)
	RETURNS bool
	LANGUAGE plpgsql
AS $function$
	begin
		update public."user" set login = login_u, about = about_u, "name" = name_u, surname = surname_u where login = etalon;
		return true;

	END;
$function$
;

